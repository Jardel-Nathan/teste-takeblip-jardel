import React from 'react';
import { BsWhatsapp } from "react-icons/bs";
import { Container, Line, TakeBlipWhatsLink, TakeBlipWhatsMain } from './styles/indexStyle';
import banner from './assets/banner.png';
import InputSection from './components/input';


function App() {
  return (
    <div className="App">

      <Container>
        <div>
          <TakeBlipWhatsLink>
            <div className="take-blip-whats-link">

              <TakeBlipWhatsMain>

                <div className="take-blip-whats-link-header">
                  <img src="https://d9hhrg4mnvzow.cloudfront.net/digital.take.net/conversas-inteligentes/c1d8cce0-logo-take-blip-vertical_105p05n000000000000028.png" alt="Take Blip WhatsApp Link" />

                  <h1>
                    Gerador de link para Whatsapp <BsWhatsapp></BsWhatsapp>
                  </h1>
                </div>


                <div className='take-blip-whats-link-input-section'>

                  <InputSection></InputSection>


                  <div className='take-blip-whats-link-how-works'>
                    <b> Como funciona </b>
                    <ul>
                      <li>1. Insira o nº do WhatsApp Ex: (99) 9 9999-9999</li>
                      <li>2. Escreva a mensagem padrão que será exibida</li>
                      <li>3. Escolha entre Desktop ou Mobile</li>
                      <li>4. Copie o link e compartilhe</li>
                    </ul>
                  </div>



                  <Line />
                  {/* <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontWeight: 'bold',
                    textDecoration: 'none',
                  }}>
                    Desenvolvido por: <a style={{
                      color: '#000',
                      textDecoration: 'none',

                    }} href='http://jardeldev.herokuapp.com/' target="_blank"> Jardel Nathan </a>
                  </div> */}

                </div>

              </TakeBlipWhatsMain>
              <div>
                <img style={{
                  width: '100%',
                  height: '100%',
                  objectFit: 'contain',
                  'background': '#21daea',
                }} src={banner} alt="" />
              </div>

            </div>
          </TakeBlipWhatsLink>

        </div>
      </Container>

    </div >
  );
}

export default App;
